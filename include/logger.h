#pragma once

#define SPDLOG_FMT_EXTERNAL
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#define SPDLOG_DEBUG_ON
#define SPDLOG_TRACE_ON

#include <spdlog/sinks/null_sink.h>
#include <spdlog/spdlog.h>

namespace tg_bot_n {

  static inline std::string indent_str(size_t count) {
    static size_t tab_size = 2;
    return std::string(count * tab_size, ' ');
  }

  struct logger_t {
    const char* file;
    int line;
    const char* function;
    static inline size_t indent = 0;
    static inline spdlog::level::level_enum level = spdlog::level::off;

    static inline const std::string name = "core";
    static inline const std::string pattern = "[%Y-%m-%d %H:%M:%S:%e] [%L] #%-5# %v";
    static inline std::shared_ptr<spdlog::logger> logger = spdlog::null_logger_mt(name);

    logger_t(const char* file, int line, const char* function);
    ~logger_t();

    static void init(const std::string& file_log, const std::string& level_str);
  };



#define TRACER tg_bot_n::logger_t logger{__FILE__, __LINE__, __PRETTY_FUNCTION__}

#define LOGGER(lvl, format, ...) \
      tg_bot_n::logger_t::logger->log(spdlog::source_loc{__FILE__, __LINE__, __FUNCTION__}, spdlog::level::lvl, \
          "{}{}: " format, tg_bot_n::indent_str(tg_bot_n::logger_t::indent), __FUNCTION__, __VA_ARGS__);
}
