#pragma once

#include <string>
#include <deque>

#include "config.h"

#include <nlohmann/json.hpp>



namespace tg_bot_n {

  struct tg_app_movie_t {
    std::string name;
    std::deque<std::string> photos;
    std::deque<std::string> more_like_names;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(tg_app_movie_t, name, photos, more_like_names);

    bool operator<(const tg_app_movie_t& tg_app_movie) const { return name < tg_app_movie.name; }
  };

  struct tg_app_movies_t {
    std::deque<tg_app_movie_t> movies;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(tg_app_movies_t, movies);
  };



  struct quiz_t {
    std::string             photo;
    std::string             question;
    std::deque<std::string> options;
    size_t                  correct_option_id;
  };



  struct tg_bot_t {
    config_t config;
    tg_app_movies_t movies;

    void init();
    void run();
    void step();

    std::string request(const std::string& host, const std::string& target) const;

    quiz_t get_quiz() const;
    void send_quiz(const quiz_t& quiz, size_t chat_id) const;

    size_t rand() const;
  };
}

