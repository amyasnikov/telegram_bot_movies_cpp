FROM gcc:10.2.0 AS image_env

ARG JOBS=1

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y cmake make libssl-dev rpm

RUN git clone --depth 1 https://github.com/fmtlib/fmt.git \
  && cd fmt \
  && cmake -S. -B./build -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
  && cmake --build ./build -j $JOBS \
  && cmake --build ./build --target install

RUN git clone --depth 1 https://github.com/gabime/spdlog.git \
  && cd spdlog \
  && cmake -S. -B./build \
  && cmake --build ./build -j $JOBS \
  && cmake --build ./build --target install

RUN git clone --depth 1 https://github.com/nlohmann/json.git \
  && cd json \
  && cmake -S. -B./build \
  && cmake --build ./build -j $JOBS \
  && cmake --build ./build --target install



FROM image_env as image_builder

COPY . /telegram_bot_movies
WORKDIR /telegram_bot_movies

RUN cmake -S. -B./build \
  && cmake --build ./build -j $JOBS \
  && cmake --build ./build -j $JOBS --target package

ENTRYPOINT ["./build/telegram_bot_movies"]



FROM ubuntu as image_compact

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y libssl-dev ca-certificates

RUN mkdir /tmp/telegram_bot_movies
COPY --from=image_builder /telegram_bot_movies/build/*.deb /tmp/telegram_bot_movies
RUN apt install /tmp/telegram_bot_movies/*.deb

ENV TGBOT_MOVIES=/opt/telegram_bot_movies/tg_app_movies

ENTRYPOINT ["/opt/telegram_bot_movies/telegram_bot"]

