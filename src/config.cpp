#include "config.h"



namespace tg_bot_n {

  void config_t::init() {
    if (auto v = std::getenv("TGBOT_LEVEL"); v) {
      level = v;
    }
    if (auto v = std::getenv("TGBOT_LOG"); v) {
      log = v;
    }

    if (auto v = std::getenv("TGBOT_TOKEN"); v) {
      token = v;
    }

    if (auto v = std::getenv("TGBOT_SLEEP"); v) {
      sleep = std::stoul(v);
    }

    if (auto v = std::getenv("TGBOT_MOVIES"); v) {
      movies = v;
    }
  }
}
