#include "bot.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <iomanip>

#include "logger.h"
#include "utils.h"

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "httplib/httplib.h"


namespace tg_bot_n {

  void tg_bot_t::init() {
    config.init();
    logger_t::init(config.log, config.level);
    LOGGER(info, "config.level:  {}", config.level);
    LOGGER(info, "config.log:    {}", config.log);
    LOGGER(info, "config.token:  {}", config.token);
    LOGGER(info, "config.sleep:  {}", config.sleep);
    LOGGER(info, "config.movies: {}", config.movies);

    auto data = str_from_file(config.movies);
    movies = nlohmann::json::parse(data);
  }

  void tg_bot_t::run() {
    TRACER;

    while (true) {
      try {
        step();
      } catch (const std::exception& e) {
        LOGGER(err, "exception: {}", e.what());
        std::cerr << "exception: " << e.what() << std::endl;
      } catch (...) {
        LOGGER(err, "exception: {}", "unknown");
        std::cerr << "exception: " << "unknown" << std::endl;
      }
      std::this_thread::sleep_for(std::chrono::seconds(config.sleep));
    }
  }

  std::string tg_bot_t::request(const std::string& host, const std::string& target) const {
    TRACER;
    LOGGER(debug, "host: {}", host);
    LOGGER(debug, "target: {}", target);

    httplib::SSLClient cli(host.c_str());

    auto res = cli.Get(target.c_str());
    if (!res) {
      LOGGER(err, "error: {}", (int) res.error());
      return {};
    }

    LOGGER(debug, "code: {}", res->status);
    LOGGER(debug, "body: {}\n{}", res->body.size(), res->body);

    return res->body;
  }

  void tg_bot_t::step() {
    static const std::string host = "api.telegram.org";
    std::string target = "/bot" + config.token + "/getUpdates";
    target += "?limit=1";
    target += "&offset=" + std::to_string(config.update_id + 1);
    
    auto body = request(host, target);
    LOGGER(debug, "body: \n{}", body);
    if (body.empty()) {
      return;
    }

    auto json = nlohmann::json::parse(body);
    LOGGER(debug, "json: \n{}", json.dump(2));

    if (!json.contains("result"))
      return;

    for (const auto& result : json.at("result")) {
      LOGGER(info, "json: \n{}", result.dump(2));
      config.update_id    = result.at("update_id");

      if (!result.contains("message"))
        break;

      const auto& message = result.at("message");
      std::string text    = message.at("text");
      size_t message_id   = message.at("message_id");
      size_t chat_id      = message.at("chat").at("id");

      LOGGER(debug, "text: {}", text);

      {
        quiz_t quiz = get_quiz();
        send_quiz(quiz, chat_id);
      }
    }
  }

  quiz_t tg_bot_t::get_quiz() const {
    const auto& movie = movies.movies.at(rand() % movies.movies.size());
    const auto& photo = movie.photos.at(rand() % movie.photos.size());
    size_t limit_like_names = 4;
    size_t limit_total = 10;

    std::deque<std::string> options = { movie.name };

    for (size_t i{}; i < std::min(limit_like_names, movie.more_like_names.size()); i++) {
      options.insert(options.end(), movie.more_like_names.at(rand() % movie.more_like_names.size()));
    }

    while (options.size() < limit_total) {
      options.insert(options.end(), movies.movies.at(rand() % movies.movies.size()).name);
    }

    std::sort(options.begin(), options.end());
    options.erase(std::unique(options.begin(), options.end()), options.end());

    size_t correct_option_id = std::distance(options.begin(), std::find(options.begin(), options.end(), movie.name));

    LOGGER(info, "name: {}", movie.name);
    LOGGER(info, "photo: {}", photo);
    for (const auto option : options) {
      LOGGER(info, "option: {}", option);
    }

    return {
      .photo = photo,
      .question = "What is the name of the movie?",
      .options = options,
      .correct_option_id = correct_option_id,
    };
  }

  void tg_bot_t::send_quiz(const quiz_t& quiz, size_t chat_id) const {
    static const std::string host = "api.telegram.org";

    {
      std::string target = "/bot" + config.token + "/sendPhoto";
      target += "?chat_id=" + std::to_string(chat_id);
      target += "&photo=" + quiz.photo;
      auto body = request(host, target);
      auto json = nlohmann::json::parse(body);
      LOGGER(info, "json: \n{}", json.dump(2));
    }

    {
      std::string target = "/bot" + config.token + "/sendPoll";
      target += "?chat_id=" + std::to_string(chat_id);
      target += "&type=quiz";
      target += "&question=" + quiz.question;
      target += "&options=" + nlohmann::json(quiz.options).dump();
      target += "&correct_option_id=" + std::to_string(quiz.correct_option_id);
      auto body = request(host, target);
      auto json = nlohmann::json::parse(body);
      LOGGER(info, "json: \n{}", json.dump(2));
    }

    {
      std::string target = "/bot" + config.token + "/sendMessage";
      target += "?chat_id=" + std::to_string(chat_id);
      target += "&text=/next";
      auto body = request(host, target);
      auto json = nlohmann::json::parse(body);
      LOGGER(info, "json: \n{}", json.dump(2));
    }
  }

  size_t tg_bot_t::rand() const {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> distrib;
    return distrib(gen);
  }
}
