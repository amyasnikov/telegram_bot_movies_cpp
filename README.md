# TELEGRAM_BOT_MOVIES

* Author: Alexander Myasnikov
* mailto: myasnikov.alexander.s@gmail.com
* git: https://gitlab.com/amyasnikov/telegram_bot_movies_cpp



## Description

* telegram bot @amyasnikov_movies_bot
* Guess the movie from the photo



## Build

```
cmake -S. -B./build && cmake --build ./build
```



## Run

```
TGBOT_MOVIES=$PWD/tg_app_movies TGBOT_TOKEN=<token> ./build/telegram_bot
```



