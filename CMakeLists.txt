cmake_minimum_required(VERSION 3.10)



execute_process(COMMAND ./version_ctl.sh version
  WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
  OUTPUT_VARIABLE VERSION
  OUTPUT_STRIP_TRAILING_WHITESPACE
  )



project(telegram_bot_movies
  VERSION 1.0.2
  LANGUAGES CXX
  )



file(GLOB SRCFILES "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")
file(GLOB_RECURSE HDRFILES "${CMAKE_CURRENT_SOURCE_DIR}/include/*")



set(FLAGS
  -Werror
  -Wall
  -Wextra
  -Wpedantic
  -Wconversion
  -Wextra-semi
  -Wfloat-equal
  -Wlogical-op
  -Wnon-virtual-dtor
  -Wold-style-cast
  -Wshadow=compatible-local
  -Wsign-conversion
  -Wsign-promo
  -Wzero-as-null-pointer-constant

  -Wcast-align
  -Wcast-qual
  -Woverloaded-virtual
  -Wredundant-decls
  )

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(fmt REQUIRED)
find_package(OpenSSL REQUIRED)
find_package(Threads REQUIRED)
set(OPENSSL_USE_STATIC_LIBS TRUE)



add_executable(telegram_bot main.cpp ${SRCFILES})
target_link_libraries(telegram_bot PUBLIC -static-libgcc -static-libstdc++ fmt::fmt OpenSSL::SSL OpenSSL::Crypto Threads::Threads)
target_include_directories(telegram_bot PUBLIC ${CMAKE_SOURCE_DIR}/include ${CMAKE_SOURCE_DIR}/3rdparty)



install(TARGETS telegram_bot
  RUNTIME DESTINATION opt/telegram_bot_movies ${CMAKE_INSTALL_BINDIR}
  )
install(FILES tg_app_movies DESTINATION opt/telegram_bot_movies)
SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Alexander Myasnikov")
SET(CPACK_GENERATOR "7Z;DEB;RPM;TBZ2;TZ;ZIP;")
set(CPACK_PACKAGING_INSTALL_PREFIX "/")
INCLUDE(CPack)

